#include <stdio.h>
#include "medfilter.h"

/*int main(){
	queuesignal_t *qraw, *qmedian;
	qraw = QueueCreate();
	qmedian = QueueCreate();
	int signal[20];
	int filtered;
	for (int i=0; i<20; i++){ 
		signal[i] = i * i;
		filtered = mixedfilter(qraw, qmedian, signal[i]);
		printf("The raw data is %d, the filtered data is %d\n", signal[i], filtered);
	}
	
}*/
/*
int main(){
	queuesignal_t *qraw;
	qraw = QueueCreate();
	//qmedian = QueueCreate();
	int signal[20];
	int filtered;
	for (int i=0; i<20; i++){ 
		signal[i] = i * i;
		if (!QueueisFull(qraw)){
			QueueEn(qraw, signal[i]);
		}
		else{
			filtered = medianfilter(qraw);
			printf("The raw data is %d with %dth iteration, the filtered data is %d\n", signal[i],i, filtered);
			QueueDe(qraw);	
			QueueEn(qraw, signal[i]);
		}
		
		
	}
	
}*/

int main(){
	queuesignal_t *qraw;
	qraw = QueueCreate();
	//qmedian = QueueCreate();
	int signal[20];
	int filtered;
	for (int i=0; i<20; i++){ 
		signal[i] = i * i;
		if (!QueueisFull(qraw)){
			QueueEn(qraw, signal[i]);
		}
		else{
			filtered = movingaverfilter(qraw);
			printf("The raw data is %d with %dth iteration, the filtered data is %d\n", signal[i],i, filtered);
			QueueDe(qraw);	
			QueueEn(qraw, signal[i]);
		}
		
		
	}
	
}





