#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#ifndef _filter_h
#define _filter_h

#ifdef __cplusplus
extern "C" {
#endif

double *trans_all(int *pix, int yaw);

//double *trans_pb();

//double *trans_all(double *H_wp, double *H_pb);
#ifdef __cplusplus
}
#endif

#endif
