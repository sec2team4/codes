#include "calibration.h"

double *trans_all(int *pix, int yaw){
	
	double *pinpon = malloc(3*sizeof(double));
	double angle_r=((double)yaw)*3.14159/180.0;	
		
	double d1 = sqrt((pow(((double)pix[0]-pix[2]),2)+pow(((double)pix[1]-pix[3]),2)));
	double d2 = sqrt((pow(((double)pix[2]-pix[4]),2)+pow(((double)pix[3]-pix[5]),2)));
	double d3 = sqrt((pow(((double)pix[4]-pix[6]),2)+pow(((double)pix[5]-pix[7]),2)));
	double d4 = sqrt((pow(((double)pix[6]-pix[0]),2)+pow(((double)pix[7]-pix[1]),2)));
	double averdist_p = (d1+d2+d3+d4)/4;
	
	double center_b_x = -2.2;
	double center_b_y = 5.199;
	double center_b_z = 0.969;
	
	double offset_x =-2.5;
	double offset_y = 5.0;
	
	double world_x_offset=-offset_x*cos(-angle_r)+offset_y*sin(-angle_r);
	double world_y_offset=-offset_x*sin(-angle_r)-offset_y*cos(-angle_r);		
		
	double f=222.211766331;
	int xc = 160;
	int yc = 100;
	double d_w = 4.242640687;
	double C = f*d_w;
	double h = C/averdist_p;
	double center[2];
	center[0] = d_w*(((double)(pix[0]+pix[2]+pix[4]+pix[6]))/4-(double)(xc))/averdist_p;
	center[1] = d_w*(((double)(pix[1]+pix[3]+pix[5]+pix[7]))/4-(double)(yc))/averdist_p;

	pinpon[0] = offset_x * cos(angle_r) - center[0] - center_b_x + offset_y * sin(angle_r);
	pinpon[1] = center[1] - center_b_y + offset_y * cos(angle_r) - offset_x * sin(angle_r);
	pinpon[2] = -center_b_z - h+8.5;
	//double *H_wp = malloc(16*sizeof(double));
	/*H_wp = [1, 0, 0, -center[0],
      0, 1, 0, -center[1],
      0, 0, 1, h,
      0, 0, 0, 1 ];*/
	
	return pinpon;
}

/*double *trans_all(double *H_wp, double *H_pb)
{
	double *pinpon = malloc(3*sizeof(double));
	pinpon[0] = -(H_wp[3]+H_pb[3]);
	pinpon[1] = -(H_wp[7]+H_pb[7]);
	pinpon[2] = -(H_wp[11]+H_pb[11]);

	return pinpon;
}

double *trans_pb()
{
	double *H_pb = malloc(16*sizeof(double));
	H_pb = [1, 0, 0, -2.185039,
	      0, 1, 0, 5.15511811,
	      0, 0, 1, 0.59370079,
	      0, 0, 0, 1];

	H_pb[0] = 1;
	H_pb[1] = 0;
	H_pb[2] = 0;
	H_pb[3] = -2.185039;
	H_pb[4] = 0;
	H_pb[5] = 1;
	H_pb[6] = 0;
	H_pb[7] = 5.15511811;
	H_pb[8] = 0;
	H_pb[9] = 0;
	H_pb[10] = 1;
	H_pb[11] = 0.59370079;
	H_pb[12] = 0;
	H_pb[13] = 0;
	H_pb[14] = 0;
	H_pb[15] = 1;
	return H_pb;
}*/
