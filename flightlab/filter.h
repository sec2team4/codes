
#include <stdio.h>
#include <inttypes.h>
#include <lcm/lcm.h>

#include "lcmtypes/pixy_frame_t.h"

#ifndef _filter_h
#define _filter_h

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _queuesignal_t queuesignal_t;
struct _queuesignal_t
{
	int data[5];
	int front;
	int count;
};

queuesignal_t *QueueCreate(void);

void QueueDestroy(queuesignal_t *queue);

void QueueEn(queuesignal_t *queue, int signal);

void QueueDe(queuesignal_t *queue);

int QueueisEmpty(queuesignal_t *queue);

int QueueisFull(queuesignal_t *queue);

int medianfilter(queuesignal_t *queue);

int movingaverfilter(queuesignal_t *queue);


#ifdef __cplusplus
}
#endif

#endif
