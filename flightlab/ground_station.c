#define EXTERN  // Needed for global data declarations in bbb.h
#include "bbblib/bbb.h" 
#include <stdbool.h>

#include <lcm/lcm.h>
#include "lcmtypes/deltaarm_t.h"
#include "util.h"

int main()
{
	const char *channel = "DELTAARM";
  // Initialize LCM
  lcm_t *lcm = lcm_create(NULL);
  deltaarm_t msg = {};
	char command;
	
	bool retrieve = false;
	bool release = false;
	bool abort = false;
	bool stow = false;
	printf("r to retrieve\nd to drop\na to abort\ns to stow\n");
  while(1) {
    printf("Command:\n");
    scanf(" %c", &command);
    if(command == 'r') retrieve = true;
    if(command == 'd') release = true;
    if(command == 'a') abort = true;
    if(command == 's') stow = true;
    msg.utime = utime_now();
		msg.retrieve = retrieve;
		msg.release = release;
		msg.abort = abort;
		msg.stow = stow;

		deltaarm_t_publish(lcm, channel, &msg);
		
		retrieve = false;
		release = false;
		abort = false;
		stow = false;
  }
  return 0;
}

