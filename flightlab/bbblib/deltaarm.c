// Code from Trossen Robotics Delta Arm site:
// http://forums.trossenrobotics.com/tutorials/introduction-129/delta-robot-kinematics-3276/
// Short main() added at the end for illustration
//
#define EXTERN
//#include <stdio.h>
//#include <math.h>
#include "bbb.h"
#include <stdbool.h>
#include <lcm/lcm.h>
#include "../util.h"
#include "../lcmtypes/deltaarm_t.h"
#include "../lcmtypes/goal_t.h"
#include "../lcmtypes/imu_t.h"
#include "../lcmtypes/locations_t.h"
#include <pthread.h> //needed for command inputs

// Delat arm function declarations
//int delta_calcForward(double theta1, double theta2, double theta3, double *x0, double *y0, double *z0);
//int delta_calcAngleYZ(double x0, double y0, double z0, double *theta);
//int delta_calcInverse(double x0, double y0, double z0, double *theta1, double *theta2, double *theta3);

////////////
typedef struct commands commands_t;
struct commands {
	bool abort;
	bool release;
	bool retrieve;
	bool stow;
};
typedef struct state state_t;
struct state {
	commands_t *commands;
	bool grabbing;
	bool holding;
	bool releasing;
	bool seeking;
	bool stowed;
	bool verbose;
	double coords[3];
	double goalCoords[3];
	double accel[3];
	double gyro[3];
	double z_stowed,z_active,z_suction,z_offset;
	
	double msg_freq;
	
	char *channel;
  lcm_t *lcm;
  locations_t msg;
  
	pthread_t command_thread;
	pthread_t goal_thread;
	pthread_t imu_thread;
	pthread_mutex_t mutex;
};
void state_set(state_t *state, bool s0, bool s1, bool s2, bool s3, bool s4){
	state->grabbing  = s0;
	state->holding   = s1;
	state->releasing = s2;
	state->seeking   = s3;	
	state->stowed    = s4;
}
void state_print(state_t* state){
	if(state->verbose){
		printf("------STATES------\n");
		printf("grabbing: %d\n",state->grabbing);
		printf("holding: %d\n",state->holding);
		printf("releasing: %d\n",state->releasing);
		printf("seeking: %d\n",state->seeking);	
		printf("stowed: %d\n",state->stowed);
		printf("-----COMMANDS-----\n");
		printf("abort: %d\n",state->commands->abort);
		printf("drop: %d\n",state->commands->release);
		printf("retrieve: %d\n",state->commands->retrieve);	
		printf("stow: %d\n",state->commands->stow);
	}
}

state_t *state_create (void){
	state_t *state = calloc (1, sizeof(*state));
	commands_t *commands = calloc(1, sizeof(*commands));
	state->commands = commands;
	
	state->channel = "LOCATIONS";
  state->lcm = lcm_create(NULL);
  state->msg.theta[0] = 0;
  state->msg.theta[1] = 0;
  state->msg.theta[2] = 0;
  state->msg.eff[0] = 0;
  state->msg.eff[1] = 0;
  state->msg.eff[2] = 0;
  state->msg.utime = 0;
	
	state->msg_freq = 100;
	
	state->z_stowed = -3.2;
	state->z_active = -4;
	state->z_suction= -1.25;
	state->z_offset = 1.75;
	state->verbose = false;
	state->coords[0]= 0;
	state->coords[0]= 0;
	state->coords[0]= -3.6;
	state->grabbing = false;
	state->holding = false;
	state->releasing = false;
	state->seeking = false;	
	state->stowed = true;
	state->commands->abort = false;
	state->commands->release = false;
	state->commands->retrieve = false;
	state->commands->stow = false;
	return state;
}



// Robot geometry (assumes identical links) 
// --> watch these globals - may want to add identifiers (e.g., "delta_f", "delta_e"...)
const double f = 1.48*2*sqrt(3);     // base reference triangle side length
const double e = 1.652*2*sqrt(3);     // end effector reference triangle side length
const double rf = 3.0;    // Top link length
const double re = 5.76;    // Bottom link length
 
// trigonometric constants
const double pi = 3.141592653;    // PI
const double sqrt3 = 1.732051;    // sqrt(3)
const double sin120 = 0.866025;   // sqrt(3)/2
const double cos120 = -0.5;        
const double tan60 = 1.732051;    // sqrt(3)
const double sin30 = 0.5;
const double tan30 = 0.57735;     // 1/sqrt(3)
const bool bbb_enabled = true;
 
// forward kinematics: (theta1, theta2, theta3) -> (x0, y0, z0)
// returned status: 0=OK, -1=non-existing position
int delta_calcForward(double theta1, double theta2, double theta3, double *x0, double *y0, double *z0) 
{
  double t = (f-e)*tan30/2;
  double dtr = pi/(double)180.0;
 
  theta1 *= dtr;
  theta2 *= dtr;
  theta3 *= dtr;
 
  double y1 = -(t + rf*cos(theta1));
  double z1 = -rf*sin(theta1);
 
  double y2 = (t + rf*cos(theta2))*sin30;
  double x2 = y2*tan60;
  double z2 = -rf*sin(theta2);
 
  double y3 = (t + rf*cos(theta3))*sin30;
  double x3 = -y3*tan60;
  double z3 = -rf*sin(theta3);
 
  double dnm = (y2-y1)*x3-(y3-y1)*x2;
 
  double w1 = y1*y1 + z1*z1;
  double w2 = x2*x2 + y2*y2 + z2*z2;
  double w3 = x3*x3 + y3*y3 + z3*z3;
     
  // x = (a1*z + b1)/dnm
  double a1 = (z2-z1)*(y3-y1)-(z3-z1)*(y2-y1);
  double b1 = -((w2-w1)*(y3-y1)-(w3-w1)*(y2-y1))/2.0;
 
  // y = (a2*z + b2)/dnm;
  double a2 = -(z2-z1)*x3+(z3-z1)*x2;
  double b2 = ((w2-w1)*x3 - (w3-w1)*x2)/2.0;
 
  // a*z^2 + b*z + c = 0
  double a = a1*a1 + a2*a2 + dnm*dnm;
  double b = 2*(a1*b1 + a2*(b2-y1*dnm) - z1*dnm*dnm);
  double c = (b2-y1*dnm)*(b2-y1*dnm) + b1*b1 + dnm*dnm*(z1*z1 - re*re);
  
  // discriminant
  double d = b*b - (double)4.0*a*c;
  if (d < 0) return -1; // non-existing point
 
  *z0 = -(double)0.5*(b+sqrt(d))/a;
  *x0 = (a1*(*z0) + b1)/dnm;
  *y0 = (a2*(*z0) + b2)/dnm;
  return 0;
}
 
// inverse kinematics
// helper functions, calculates angle theta1 (for YZ-plane)
int delta_calcAngleYZ(double x0, double y0, double z0, double *theta) {
  double y1 = -0.5 * 0.57735 * f; // f/2 * tg 30
  y0 -= 0.5 * 0.57735    * e;    // shift center to edge
  // z = a + b*y
  double a = (x0*x0 + y0*y0 + z0*z0 +rf*rf - re*re - y1*y1)/(2*z0);
  double b = (y1-y0)/z0;
  // discriminant
  double d = -(a+b*y1)*(a+b*y1)+rf*(b*b*rf+rf); 
  if (d < 0) return -1; // non-existing point
  double yj = (y1 - a*b - sqrt(d))/(b*b + 1); // choosing outer point
  double zj = a + b*yj;
  *theta = 180.0*atan(-zj/(y1 - yj))/pi + ((yj>y1)?180.0:0.0);
  return 0;
}
 
// inverse kinematics: (x0, y0, z0) -> (theta1, theta2, theta3)
// returned status: 0=OK, -1=non-existing position
int delta_calcInverse(double x0, double y0, double z0, double *theta1, double *theta2, double *theta3) {
  *theta1 = *theta2 = *theta3 = 0;
  int status = delta_calcAngleYZ(x0, y0, z0, theta1);
  if (status == 0) status = delta_calcAngleYZ(x0*cos120 + y0*sin120, y0*cos120-x0*sin120, z0, theta2);  // rotate coords to +120 deg
  if (status == 0) status = delta_calcAngleYZ(x0*cos120 - y0*sin120, y0*cos120+x0*sin120, z0, theta3);  // rotate coords to -120 deg
  return status;
}

// ------------------------------------------------- LCM Interfacing --------------------------------------------------------

void deltaarm_handler(const lcm_recv_buf_t *rbuf, const char *channel,
                   const deltaarm_t *msg, void *userdata){
  state_t * state = userdata;
  //state_print(state);
	//printf("Received message on channel %s, timestamp %f\n",channel, (double)msg->utime);
	
  // Read LCM Message
	state->commands->retrieve = msg-> retrieve;
	state->commands->abort = msg-> abort;
	state->commands->stow = msg-> stow;
	state->commands->release = msg-> release;
	
	// State Machine [grabbing holding releasing seeking stowed]
	
	if(state->commands->stow){
		if(state->grabbing || state->releasing){
			// What do? Wait until finished grabbing or releasing?
		} else state_set(state,0,0,0,0,1);
	} else
	if(state->commands->abort){
		if(!(state->stowed)) state_set(state,0,state->holding,0,0,0);
	} else
	if(state->commands->retrieve){
		state_set(state,0,0,0,1,0);
	} else
	if(state->commands->release){
		state_set(state,0,1,0,1,0);
	}
	//state_print(state);
}
void goal_handler(const lcm_recv_buf_t *rbuf, const char *channel,
                   const goal_t *msg, void *userdata){
  state_t * state = userdata;
  //state_print(state);
	//printf("Received message on channel %s, timestamp %f\n",channel, (double)msg->utime);
	
  // Read LCM Message
	state->goalCoords[0] = msg-> goal_x;
	state->goalCoords[1] = msg-> goal_y;
	state->goalCoords[2] = msg-> goal_z;
	//printf("Goal from Pixy: %7.3f, %7.3f, %7.3f\n",state->goalCoords[0],state->goalCoords[1],state->goalCoords[2]);
}
void imu_handler(const lcm_recv_buf_t *rbuf, const char *channel,
                   const imu_t *msg, void *userdata){
  state_t * state = userdata;
  //state_print(state);
	//printf("Received message on channel %s, timestamp %f\n",channel, (double)msg->utime);
	
  // Read LCM Message
	state->accel[0] = msg-> accel[0];
	state->accel[1] = msg-> accel[1];
	state->accel[2] = msg-> accel[2];
	state->gyro[0] = msg-> gyro[0];
	state->gyro[1] = msg-> gyro[1];
	state->gyro[2] = msg-> gyro[2];
	if(state->verbose) printf("IMU Data: %7.3f, %7.3f, %7.3f, %7.3f, %7.3f, %7.3f\n",state->accel[0],state->accel[1],state->accel[2],state->gyro[0],state->gyro[1],state->gyro[2]);
}

void * command_loop(void *user){
	lcm_t *lcm = lcm_create(NULL);
	deltaarm_t_subscribe(lcm,"DELTAARM",deltaarm_handler, user);
	while(1){
		lcm_handle(lcm);
	}
}
void * goal_loop(void *user){
	lcm_t *lcm1 = lcm_create(NULL);
	goal_t_subscribe(lcm1,"GOAL",goal_handler, user);
	while(1){
		lcm_handle(lcm1);
	}
}
void * imu_loop(void *user){
	lcm_t *lcm2 = lcm_create(NULL);
	imu_t_subscribe(lcm2,"IMUFILTERED",imu_handler, user);
	while(1){
		lcm_handle(lcm2);
	}
}

// -------------------------------------- Manipulator Functions --------------------------------------------

int goToCoords(state_t *state, double goalCoords0, double goalCoords1, double goalCoords2, bool move){
	double theta[3];
	int stat, i, j;
	bool has_moved = false;
	double offset[3] = {7.9, 8.0, 7.6}; // zero offsets of servos
	if(goalCoords2 > -3.2){
		printf("Z must be less than -3.2\n");
		return 0;
	}
	
	stat = delta_calcInverse(goalCoords0, goalCoords1, goalCoords2, theta, (theta+1), (theta+2));
	if (stat) {
    if(state->verbose) printf("Error computing inverse kinematics - no solution exists.\n");
    return 0;
  } else if(move){
  	if(abs(state->gyro[0])>=30 || abs(state->gyro[1])>=30){
			if(state->verbose) printf("Quadrotor is UNSTABLE!!\n");
			return 0;
		} else if(!(state->coords[0]==goalCoords0 &&
							  state->coords[1]==goalCoords1 &&
							  state->coords[2]==goalCoords2)){
  		
			state->coords[0]=goalCoords0;
			state->coords[1]=goalCoords1;
			state->coords[2]=goalCoords2;
			double theta_max = 0;
			has_moved = true;
			
			for (j=0; j<3; j++){
				if(theta[j]>theta_max && &theta[j]!=NULL) theta_max = theta[j];
				//printf("theta%d:%7.3f\n",j,theta[j]);
				if(bbb_enabled){
					double  pwmperc = theta[j] * 0.05 + offset[j];
					bbb_setPeriodPWM(j, 20000000);  // Period is in nanoseconds
					bbb_setDutyPWM(j, pwmperc);  // Duty cycle percent (0-100)
			 		bbb_setRunStatePWM(j, pwm_run);
			 		}
				//printf("Running PWM at %.1f percent...\n",bbb_getDutyPWM(PWM_CHOICE));
			}
			if(bbb_enabled && state->stowed){ //set grabber
				bbb_setPeriodPWM(3, 20000000);  // Period is in nanoseconds
				bbb_setDutyPWM(3, 5.0);  // Duty cycle percent (0-100)
				bbb_setRunStatePWM(3, pwm_run);
			}
			if(!bbb_enabled) printf("EFF at %7.3f,%7.3f,%7.3f\n",goalCoords0,goalCoords1,goalCoords2);
			usleep((unsigned int)(.5*theta_max*(.13/60.0)*1000000.0)); // Sleep for the longest angle change of the 3 servos
		}
		// Publish to LCM
		if( has_moved || (utime_now() > (state->msg.utime + (unsigned int)1000000.0/(state->msg_freq)))){ // If it moved or if the last update was .01s ago or older
			state->msg.utime = utime_now();
			state->msg.theta[0] = theta[0];
			state->msg.theta[1] = theta[1];
			state->msg.theta[2] = theta[2];
			state->msg.eff[0] = goalCoords0;
			state->msg.eff[1] = goalCoords1;
			state->msg.eff[2] = goalCoords2;
	

			locations_t_publish(state->lcm, state->channel, &(state->msg));
		}
	}
	
	
	return 1;
}
void grab_actuate(bool grab){
	double pwmperc = 5.0;
	if(grab) pwmperc = 10.0;
	bbb_setPeriodPWM(3, 20000000);  // Period is in nanoseconds
	bbb_setDutyPWM(3, pwmperc);  // Duty cycle percent (0-100)
	bbb_setRunStatePWM(3, pwm_run);
	usleep((unsigned int)(.5*1000000.0));
}
void grab(state_t* state, double goalCoords[3], bool grab){
	double pwmperc;
	state->seeking = false;
	double i,steps=3.0;
	for(i=1.0;i<=steps;i++){
		goToCoords(state,goalCoords[0],goalCoords[1],goalCoords[2]+(state->z_suction)*(i/steps),true);
	}
	if(grab){
		//pwmperc = 10.0; //grabbing position
		state->holding = true;
		state->grabbing = true;
	}
	else{
		//pwmperc = 5.0;      //releasing position
		state->holding = false;
		state->releasing = true;
	}
	if(bbb_enabled){
		grab_actuate(grab);
		//bbb_setPeriodPWM(3, 20000000);  // Period is in nanoseconds
		//bbb_setDutyPWM(3, pwmperc);  // Duty cycle percent (0-100)
		//bbb_setRunStatePWM(3, pwm_run);
	} else {
		printf("------------------------------GRABBING------------------------------\n");
	}
	//sleep(1);
	goToCoords(state,goalCoords[0],goalCoords[1],goalCoords[2],true);
	state->releasing = false;
	state->grabbing = false;
	state_print(state);
}

// ---------------------------------------------- Main ----------------------------------------------------

int main()
{
	int j;
	double goalCoords[3];
	double r,z,dx,dy,dr,dz,fttr,fttz,q,sqa,a,b,norm;
	double kr=1,kz=5,kt=1,l=4,h=2; // Trajectory params, use matlab to verify
	const double seek_step = .25, seek_threshold[3] = {.1,.1,.25};
	// Initialize bbb board and pwm pins
	if(bbb_enabled){
		if (bbb_init()) {
			printf("Error initializing BBB.\n");
			return -1;
		}
		for(j=0;j<4;j++){
			if (bbb_initPWM(j)) {
			 	printf("Error initializing BBB PWM pin %d. Are you running as root?\n", j);
			 	return -1;
			}
		}
  }
  if(0){
		printf("Please input the position of end-effector.\n");
		printf("x=\n");
		scanf("%lf", &goalCoords[0]);
		printf("y=\n");
		scanf("%lf", &goalCoords[1]);
		printf("z=\n");
		scanf("%lf", &goalCoords[2]);
	}
	state_t *state = state_create();
	//printf("one\n");
	//state_print(state);
	pthread_create (&state->command_thread, NULL, command_loop, state);
	pthread_create (&state->goal_thread, NULL, goal_loop, state);
	pthread_create (&state->imu_thread, NULL, imu_loop, state);
	
  
	while(1){
		if(state->stowed){
			goToCoords(state,0,0,state->z_stowed,true);
		}
		else{
			goToCoords(state,0,0,state->z_active,true);
			if(state->seeking){
				if(state->verbose) printf("seeking goal\n");
				bool atGoal = false;
				bool action = state->commands->retrieve; // true for retrieve, false for drop
				if(action) grab_actuate(false);
				while(state->seeking){
				
					//Get xyz goalCoords over lcm or something
					goalCoords[0] = state->goalCoords[0];
					goalCoords[1] = state->goalCoords[1];
					goalCoords[2] = state->goalCoords[2] + (state->z_offset); // Add suction cup height
					if(goToCoords(state,goalCoords[0],goalCoords[1],(goalCoords[2] + state->z_suction),false)){ //check if goal is reachable
						if(state->verbose) printf("-------------------------------------------------------------------\n");
						if(state->verbose) printf("moving from %7.3f, %7.3f, %7.3f, to %7.3f, %7.3f, %7.3f\n",state->coords[0],state->coords[1],state->coords[2],goalCoords[0],goalCoords[1],goalCoords[2]);
						//Move towards goal coords
						r = sqrt(pow(goalCoords[0]-state->coords[0],2.0) + pow(goalCoords[1]-state->coords[1],2.0)); // xy distance to target
						z = state->coords[2] - goalCoords[2];
						//printf("r,z: %7.3f, %7.3f\n",r,z);
						fttr = -kr*(r-l*pow(z/h,2.0));
						fttz = -kz*(z-h*sqrt(r/l));
						if(fttz<0){
							 q = -2*z*sqrt(l)/h;
							 sqa = (-q - sqrt(pow(q,2.0) - 4*r))/2;
							 b = h*sqa/sqrt(l);
							 a = pow(sqa,2.0);
							 dr = a-r;
							 dz = b-z;
							 //printf("params: %7.3f, %7.3f, %7.3f, %7.3f, %7.3f\n",q,sqa,a,b,dr);
						} else {
							dr = -kt*2*l*z/pow(h,2.0) + fttr;
							dz = -kt+fttz;
						}
						if(r!=0){
							dx = dr*(state->coords[0]-goalCoords[0])/r;
							dy = dr*(state->coords[1]-goalCoords[1])/r;
						} else {
							dx = 0;
							dy = 0;
						}
						norm = sqrt(pow(dx,2.0) + pow(dy,2.0) + pow(dz,2.0)) / seek_step;
						//printf("Deltas: %7.3f, %7.3f, %7.3f, %7.3f\n",dx,dy,dz,norm);
						dx /= norm; dy /= norm; dz /= norm;
						
						//printf("Deltas: %7.3f, %7.3f, %7.3f\n",dx,dy,dz);
						goToCoords(state, state->coords[0] + dx, state->coords[1] + dy, state->coords[2] + dz, true);
						//printf("check from %7.3f, %7.3f, %7.3f, to %7.3f, %7.3f, %7.3f\n",state->coords[0],state->coords[1],state->coords[2],goalCoords[0],goalCoords[1],goalCoords[2]);
						
						//Check if at goal, atGoal = testFN()
						atGoal = (fabs(goalCoords[0]-state->coords[0])<seek_threshold[0]) && 
										 (fabs(goalCoords[1]-state->coords[1])<seek_threshold[1]) &&
										 (fabs(goalCoords[2]-state->coords[2])<seek_threshold[2]);
						if(atGoal){
							grab(state,goalCoords,action);
						}
					} else goToCoords(state, state->coords[0], state->coords[1], state->coords[2], true);
				}
			}
		} 
		
	}
	
	
	
	
	
  // Define a simple case of pointing (almost) straight down
  double E[3];
  double theta[3];
  int stat, i;
 printf("Please input the position of end-effector.\n");
 printf("x=\n");
 scanf("%lf", &E[0]);
 printf("y=\n");
 scanf("%lf", &E[1]);
 printf("z=\n");
 scanf("%lf", &E[2]);
  // Run inverse kinematics to solve for angles (theta)
  stat = delta_calcInverse(E[0], E[1], E[2], theta, (theta+1), (theta+2));
  if (stat) {
    printf("Error computing inverse kinematics - no solution exists.\n");
    return -1;
  }

  printf("Inverse kinematics solution:  theta = {%lf, %lf, %lf}\n",
	 theta[0], theta[1], theta[2]);
	if (bbb_init()) {
    		printf("Error initializing BBB.\n");
    		return -1;
  	}

	double offset[3] = {7.9, 8.0, 7.6};

	for (j=0; j<3; j++){

		double  pwmperc = theta[j] * 0.05 + offset[j];
                //int perc_temp= (int)pwmperc;
		int PWM_CHOICE = j;
		if (bbb_initPWM(PWM_CHOICE)) {
    			printf("Error initializing BBB PWM pin %d. Are you running as root?\n", PWM_CHOICE);
    			return -1;
  		}


		bbb_setPeriodPWM(PWM_CHOICE, 20000000);  // Period is in nanoseconds

  		bbb_setDutyPWM(PWM_CHOICE, pwmperc);  // Duty cycle percent (0-100)

  		bbb_setRunStatePWM(PWM_CHOICE, pwm_run);



  		//printf("Running PWM at %.1f percent for 2 seconds...\n",bbb_getDutyPWM(PWM_CHOICE));

	}

	

  	sleep(1);  // Run for 2 seconds
	 	 if (bbb_initPWM(3)) {
                       // printf("Error initializing BBB PWM pin %d. Are you running as root?\n", PWM_CHOICE);
                        return -1;
                }

	        bbb_setPeriodPWM(3, 20000000);  // Period is in nanose$

                bbb_setDutyPWM(3, 10.0);  // Duty cycle percent (0-$

                bbb_setRunStatePWM(3, pwm_run);



                //printf("Running PWM at %.1f percent for 2 seconds...\n",

               /* bbb_getDutyPWM(3);
		sleep(5);


                bbb_setPeriodPWM(3, 20000000);  // Period is in nanose$

                bbb_setDutyPWM(3, 5.0);  // Duty cycle percent (0-$

                bbb_setRunStatePWM(3, pwm_run);
		*/


                //printf("Running PWM at %.1f percent for 2 seconds...\n",

                bbb_getDutyPWM(3);

		sleep(2);

  // Now run forward kinematics - should return original E values
  stat = delta_calcForward(-90, -90, -90, E, (E+1), (E+2));
  if (stat) {
    printf("Error computing forward kinematics - invalid joint vector.\n");
    return -1;
  }
  
  printf("Forward kinematics solution:  E = {%lf, %lf, %lf}\n",
	 E[0], E[1], E[2]);






  return -1;
}
/*
  double cubic_poly(double *initial,double *final,double tf)
  {
	double *outcome=malloc(12*sizeof(double));
	double T_matrix=[1, 0, 0, 0, 
		 0, 1, 0, 0,
		 1, tf, pow(tf,2), pow(tf,3),
		 0, 1, 2*tf, 3*pow(tf,2)];
	double target1[4]={initial[0], 0, final[0], 0};
	double target2[4]={initial[1], 0, final[1], 0};	
	double target3[4]={initial[2], 0, final[2], 0};
	int signum;
	gsl_vector *x=gsl_vector_alloc(4);
	gsl_permutation *perm=gsl_permutation_alloc(4);

	gsl_matrix_view a=gsl_matrix_view_array(T_matrix,4,4);
	gsl_vector_view b=gsl_vector_view_array(target1,4);
	gsl_vector_view c=gsl_vector_view_array(target2,4);
	gsl_vector_view d=gsl_vector_view_array(target3,4);
	

	gsl_linalg_LU_decomp(&a.matrix,perm,&signum);
	gsl_linalg_LU_solve(&a.matrix,perm,&b.vector,x);
	for(int i=0;i<4;i++)
	{
	  outcome[i]=gsl_vector_get(x,i)
	}
	gsl_linalg_LU_solve(&a.matrix,perm,&c.vector,x);
	for(int i=0;i<4;i++)
	{
	  outcome[i+4]=gsl_vector_get(x,i)
	}
	gsl_linalg_LU_solve(&a.matrix,perm,&d.vector,x);
	for(int i=0;i<4;i++)
	{
	  outcome[i+8]=gsl_vector_get(x,i)
	}
	return outcome;
  }
  
  */
