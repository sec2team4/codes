 /*
  *  Beaglebone Black Hardware Interface Function Library
  *  For Use in Robotics 550, University of Michigan
  *
  *  References:  
  *      http://beagleboard.org/
  *      BlackLib: http://free.com/projects/blacklib
  *      https://www.nordevx.com/content/lsm9ds0-accelerometer-magnetometer-and-gyro-one
  *
  *  bbb_test_I2C.c:  Code to read and print data from
  *  the Adafruit LSM9DS0 gyro/accelerometer/magnetometer IMU
  * 
  *  LSM9DS0 I2C address options:
  *  1. SA0 at Vdd:   accel/mag add = 0x3A;  gyro add = 0xD6
  *  2. SA0 at GND:   accel/mag add = 0x3C;  gyro add = 0xD4
  */ 

//////  WAIT TO DOWNLOAD - WILL UPDATE!!! ///////

#define EXTERN  // Needed for global data declarations in bbb.h
#include "bbb.h" 

// Local functions
void read_gyro(struct I2C_data *i2cdata, double g[]);
void read_accel(struct I2C_data *i2cdata, double a[]);
void read_mag(struct I2C_data *i2cdata, double m[]);
void read_temp(struct I2C_data *i2cdata, double *temp);


int main()
{
  struct I2C_data i2cd_gyro, i2cd_accelmag;
  int i;
  byte buf[10];
  double gyro[3], accel[3], mag[3], temperature;

  // Initialize BBB & I2C ports

  if (bbb_init()) {
    printf("Error initializing BBB.\n");
    return -1;
  }

  i2cd_gyro.name = I2C_1;  // I2C2 is enumerated as 1 on the BBB unless I2C1 is enabled
  i2cd_gyro.address = 0xD6; // Assumes SA0 is tied to VDD
  i2cd_gyro.flags = O_RDWR;  

  i2cd_accelmag.name = I2C_1;  // Same I2C port as gyro
  i2cd_accelmag.address = 0x3A; // Assumes SA0 is tied to VDD
  i2cd_accelmag.flags = O_RDWR;  
  usleep(1000);

  if (bbb_initI2C(&i2cd_gyro)) {
    printf("Error initializing I2C port %d.\n", (int) (i2cd_gyro.name));
    return -1;
  }
  printf("fd = %d\n", i2cd_gyro.fd);

  usleep(1000);

  // Only need to open I2C2 port once (no need to reopen for i2cd_accelmag)
  printf("port open\n");

  // Activate accelerometer, magnetometer, and gyro

  buf[0] = 0x20; buf[1] = 0x37;  // Activate accel/mag through control register 1  
  buf[2] = 0x24; buf[3] = 0x88;  // Activate temperature through control register 5
  buf[4] = 0x26; buf[5] = 0x00;  // Send 0x00 to control register 7

  printf("accelmag.\n");
  //bbb_writeI2C(&i2cd_accelmag, buf, 6); 
  //usleep(1000);
  
buf[0] = 0x20; buf[1] = 0x0F; 
  printf("gyro.\n");
  bbb_writeI2C(&i2cd_gyro, buf, 2);
  usleep(1000);

  // Read & print I2C data from 9DOF IMU 10 times (1 second delay)
    for (i=0;i<10;i++) {
    printf("in loop...\n");

   read_gyro(&i2cd_gyro, gyro);
    printf("i=%d, gyro = (%lf, %lf, %lf), \n", i, gyro[0], gyro[1], gyro[2]);
    usleep(1000);
//	printf("start printing the acceleration data\n");
  //      read_accel(&i2cd_accelmag, accel);
   // printf("accel = (%lf, %lf, %lf), ", accel[0], accel[1], accel[2]);
    //usleep(1000);
    
    // read_mag(&i2cd_accelmag, mag);
    //printf("mag = (%lf, %lf, %lf), ", mag[0], mag[1], mag[2]);
    //usleep(1000);

    // read_temp(&i2cd_accelmag, &temperature);
    //printf("temp = %lf ", temperature);

    sleep(1);
    }
  
  bbb_deinitI2C(&i2cd_gyro);  // Only needed for the one file descriptor / handle
  //bbb_deinitI2C(&i2cd_accelmag);
  return 0;
}

//////////////////////////////////////////
void read_gyro(struct I2C_data *i2cd, double gyro[])
{
  int tempint;
  byte buf, lobyte, hibyte;  // Used to store I2C data
  
  buf = 0x28; // X gyro, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &lobyte, 1);
  usleep(1000);
  buf = 0x29; // X gyro, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &hibyte, 1);
  usleep(1000);
  tempint = (hibyte << 8) | lobyte;
  gyro[0] = 0.00875*(tempint+110);  // 110 is the zero bias/offset - please adjust
  
  buf = 0x2A; // Y gyro, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &lobyte, 1);
  usleep(1000);
  buf = 0x2B; // Y gyro, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &hibyte, 1);
  usleep(1000);
  tempint = (hibyte << 8) | lobyte;
  gyro[1] = 0.00875*(tempint+110);  // 110 is the zero bias/offset - please adjust
  
  buf = 0x2C; // Z gyro, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &lobyte, 1);
  usleep(1000);
  buf = 0x2D; // Z gyro, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  usleep(1000);
  bbb_readI2C(i2cd, &hibyte, 1);
  usleep(1000);
  tempint = (hibyte << 8) | lobyte;
  gyro[2] = 0.00875*(tempint+110);  // 110 is the zero bias/offset - please adjust

  return;
}


void read_accel(struct I2C_data *i2cd, double accel[])
{
  int tempint;
  byte buf, lobyte, hibyte;  // Used to store I2C data
  
  buf = 0x28; // X accel, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x29; // X accel, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (hibyte << 8) | lobyte;
  accel[0] = 0.000061*tempint;    // Not sure about negative readings yet???
  
  buf = 0x2A; // Y accel, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x2B; // Y accel, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (hibyte << 8) | lobyte;
  accel[1] = 0.000061*tempint;
  
  buf = 0x2C; // Z accel, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x2D; // Z accel, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (hibyte << 8) | lobyte;
  accel[2] = 0.000061*tempint; 

  return;
}

void read_mag(struct I2C_data *i2cd, double mag[])
{
  int tempint;
  byte buf, lobyte, hibyte;  // Used to store I2C data
  
  buf = 0x80; // X mag, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x81; // X mag, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (hibyte << 8) | lobyte;
  mag[0] = 0.00008*tempint;    // Not sure about negative readings yet???
  
  buf = 0x82; // Y mag, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x83; // Y mag, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (hibyte << 8) | lobyte;
  mag[1] = 0.00008*tempint;
  
  buf = 0x84; // Z mag, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x85; // Z mag, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (hibyte << 8) | lobyte;
  mag[2] = 0.00008*tempint; 

  return;
}
  
void read_temp(struct I2C_data *i2cd, double *temp)
{
  int tempint;
  byte buf, lobyte, hibyte;  // Used to store I2C data

  buf = 0x05; // Z mag, low byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &lobyte, 1);
  buf = 0x06; // Z mag, high byte request
  bbb_writeI2C(i2cd, &buf, 1);
  bbb_readI2C(i2cd, &hibyte, 1);
  tempint = (hibyte << 8) | lobyte;
  *temp = (double) tempint; 

  return;
}


