// Code from Trossen Robotics Delta Arm site:
// http://forums.trossenrobotics.com/tutorials/introduction-129/delta-robot-kinematics-3276/
// Short main() added at the end for illustration
//
#define EXTERN
#include <stdio.h>
//#include <math.h>
#include <stdbool.h>
#include <lcm/lcm.h>
#include "../lcmtypes/deltaarm_t.h"
#include "../lcmtypes/pixy_frame_t.h"
#include "../lcmtypes/goal_t.h"
#include "../lcmtypes/imu_t.h"
#include "../lcmtypes/locations_t.h"
#include <pthread.h> //needed for command inputs

// ------------------------------------------------- LCM Interfacing --------------------------------------------------------

void deltaarm_handler(const lcm_recv_buf_t *rbuf, const char *channel,
                   const deltaarm_t *msg, void *userdata){
  FILE *outfile = userdata;
	fprintf(outfile,"%d\t%d\t%d\t%d\t\n",msg->retrieve,msg->abort,msg->stow,msg->release);
	fflush(outfile);
}
void goal_handler(const lcm_recv_buf_t *rbuf, const char *channel,
                   const goal_t *msg, void *userdata){
  FILE *outfile = userdata;
	fprintf(outfile,"%f\t%f\t%f\t\n",msg->goal_x,msg->goal_y,msg->goal_z);
	fflush(outfile);
}
void imufiltered_handler(const lcm_recv_buf_t *rbuf, const char *channel,
                   const imu_t *msg, void *userdata){
  FILE *outfile = userdata;
	fprintf(outfile,"%f\t%f\t%f\t%f\t%f\t%f\t\n",msg->accel[0],msg->accel[1],msg->accel[2],msg->gyro[0],msg->gyro[1],msg->gyro[2]);
	fflush(outfile);
}
void imu_handler(const lcm_recv_buf_t *rbuf, const char *channel,
                   const imu_t *msg, void *userdata){
  FILE *outfile = userdata;
	fprintf(outfile,"%f\t%f\t%f\t%f\t%f\t%f\t\n",msg->accel[0],msg->accel[1],msg->accel[2],msg->gyro[0],msg->gyro[1],msg->gyro[2]);
	fflush(outfile);
}
void locations_handler(const lcm_recv_buf_t *rbuf, const char *channel,
                   const locations_t *msg, void *userdata){
  FILE *outfile = userdata;
	fprintf(outfile,"%f\t%f\t%f\t%f\t%f\t%f\t%f\t\n",msg->utime,msg->theta[0],msg->theta[1],msg->theta[2],msg->eff[0],msg->eff[1],msg->eff[2]);
	fflush(outfile);
}


void * goal_loop(void *user){
	printf("2");
	lcm_t *lcm1 = lcm_create(NULL);
	FILE *outfile;
	if ((outfile = fopen("GOAL.txt","w")) == NULL) {
    printf("Error opening output file.\n");  
    return -1;
  }
	goal_t_subscribe(lcm1,"GOAL",goal_handler, outfile);
	
	while(1){
		lcm_handle(lcm1);
	}
	fclose(outfile);
}
void * imufiltered_loop(void *user){
	printf("1");
	lcm_t *lcm2 = lcm_create(NULL);
	FILE *outfile;
	if ((outfile = fopen("IMUFILTERED.txt","w")) == NULL) {
    printf("Error opening output file.\n");  
    return -1;
  }
	imu_t_subscribe(lcm2,"IMUFILTERED",imufiltered_handler, outfile);
	
	while(1){
		lcm_handle(lcm2);
	}
	fclose(outfile);
}
void * imu_loop(void *user){
	printf("1");
	lcm_t *lcm4 = lcm_create(NULL);
	FILE *outfile;
	if ((outfile = fopen("IMU.txt","w")) == NULL) {
    printf("Error opening output file.\n");  
    return -1;
  }
	imu_t_subscribe(lcm4,"IMU",imu_handler, outfile);
	
	while(1){
		lcm_handle(lcm4);
	}
	fclose(outfile);
}
void * locations_loop(void *user){
	printf("1");
	lcm_t *lcm3 = lcm_create(NULL);
	FILE *outfile;
	if ((outfile = fopen("LOCATIONS.txt","w")) == NULL) {
    printf("Error opening output file.\n");  
    return -1;
  }
	locations_t_subscribe(lcm3,"LOCATIONS",locations_handler, outfile);
	
	while(1){
		lcm_handle(lcm3);
		//printf("test\n");
	}
	fclose(outfile);
}
// ---------------------------------------------- Main ----------------------------------------------------

int main()
{

	pthread_t goal_thread;
	pthread_t imu_thread;
	pthread_t imufiltered_thread;
	pthread_t locations_thread;
	pthread_create (&goal_thread, NULL, goal_loop, NULL);
	pthread_create (&imu_thread, NULL, imu_loop, NULL);
	pthread_create (&imufiltered_thread, NULL, imufiltered_loop, NULL);
	pthread_create (&locations_thread, NULL, locations_loop, NULL);
	
	lcm_t *lcm = lcm_create(NULL);
	FILE *outfile;
	if ((outfile = fopen("DELTAARM.txt","w")) == NULL) {
    printf("Error opening output file.\n");  
    return -1;
  }
	deltaarm_t_subscribe(lcm,"DELTAARM",deltaarm_handler, outfile);
	
	while(1){
		lcm_handle(lcm);
	}
	fclose(outfile);

}
