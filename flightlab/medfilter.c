#include "medfilter.h"


queuesignal_t *QueueCreate(void){
	queuesignal_t *queue;
	queue = calloc(1,sizeof(queuesignal_t));
	queue->front = 0;
	queue->count = 0;

	return queue;
}

void QueueDestroy(queuesignal_t *queue){
	free(queue);
}

void QueueEn(queuesignal_t *queue, int signal){
	int index;
	index = (queue->front + queue->count) % 5;
	queue->data[index] = signal;
	queue->count++;
}

void QueueDe(queuesignal_t *queue){
	//int oldElement = queue->data[queue->front];
	queue->front++;
	queue->front %= 5;
	queue->count--;

	//return oldElement; 
}

int QueueisEmpty(queuesignal_t *queue){
	return queue->count <= 0;
}

int QueueisFull(queuesignal_t *queue){
	return queue->count >= 5;
}

int medianfilter(queuesignal_t *queue){
	int window[5];
	for (int i=0; i<5; i++){
		window[i] = queue->data[i];
	}
	for (int j=0; j<4; j++){
		int min = j;
		for (int k=j+1; k<5; k++)
			if (window[k] < window[min])
			min = k;
		int temp = window[j];
		window[j] = window[min];
		window[min] = temp;
			
	}
	return window[2];
}

int movingaverfilter(queuesignal_t *queue){
	int aver;
	aver = (queue->data[0] + queue->data[1] + queue->data[2] + queue->data[3] + queue->data[4])/5;
	return aver;
}

int mixedfilter(queuesignal_t *qraw, queuesignal_t *qmedian, int signal){
	int aver, rflag = 0, mflag = 0;
	QueueEn(qraw, signal);
	if (QueueisFull(qraw)){
		rflag = 1;
		int mediantemp = medianfilter(qraw);
			QueueEn(qmedian, mediantemp);
		if (QueueisFull(qmedian)){
			mflag = 1;
			aver = movingaverfilter(qmedian);
			QueueDe(qmedian);
			//QueueEn(qmedian, mediantemp);	
		}
		QueueDe(qraw);
		//QueueEn(qraw, signal);	
	}
	//QueueEn(qraw,signal);
	if(!rflag||!mflag){
		return NULL;
	}
	else 
	return aver;
}


