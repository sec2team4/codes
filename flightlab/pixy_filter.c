/* Example of receiving LCM messages */
#define EXTERN 
#include <stdio.h>
#include <inttypes.h>
#include <lcm/lcm.h>
#include "util.h"
#include <pthread.h> //needed for command inputs

#include "bbblib/bbb.h" 
#include "lcmtypes/pixy_frame_t.h"
#include "lcmtypes/goal_t.h"
#include "lcmtypes/imu_t.h"
#include "medfilter.h"
#include "calibration.h"


//Global parameters
/*const double H_pb[]=[1, 0, 0, -2.185039,
	      0, 1, 0, 5.15511811,
	      0, 0, 1, 0.59370079,
	      0, 0, 0, 1];*/


typedef struct state state_t;
struct state {
	pixy_t *pixy;
	lcm_t *lcm;
	pixy_frame_t *pixy_frame;
	imu_t *imu;
	
	char *channel;
  lcm_t *lcm_out;
  goal_t msg_out;
  char *channel2;
  lcm_t *lcm_out2;
  imu_t msg_out2;
  
	queuesignal_t *qraw_angle1, *qmedian_angle1, *qraw_height1, *qmedian_height1, *qraw_x1, *qmedian_x1, *qraw_y1, *qmedian_y1, *qraw_width1, *qmedian_width1;
	queuesignal_t *qraw_angle2, *qmedian_angle2, *qraw_height2, *qmedian_height2, *qraw_x2, *qmedian_x2, *qraw_y2, *qmedian_y2, *qraw_width2, *qmedian_width2;
	queuesignal_t *qraw_angle3, *qmedian_angle3, *qraw_height3, *qmedian_height3, *qraw_x3, *qmedian_x3, *qraw_y3, *qmedian_y3, *qraw_width3, *qmedian_width3;
	queuesignal_t *qraw_angle4, *qmedian_angle4, *qraw_height4, *qmedian_height4, *qraw_x4, *qmedian_x4, *qraw_y4, *qmedian_y4, *qraw_width4, *qmedian_width4;
	queuesignal_t *qraw_accel1, *qraw_accel2, *qraw_accel3, *qraw_gyro1, *qraw_gyro2, *qraw_gyro3;
	queuesignal_t *qmedian_accel1, *qmedian_accel2, *qmedian_accel3, *qmedian_gyro1, *qmedian_gyro2, *qmedian_gyro3; 
	
	pthread_t imu_thread;
	pthread_mutex_t mutex;
};

void pixy_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const pixy_frame_t *msg, void *userdata)
{
	state_t *state = userdata;
   // printf("Received message on channel %s, timestamp %" PRId64 "\n", channel, msg->utime);
	state->pixy_frame = msg;
    printf("Number of objects: %d\n", msg->nobjects);
	int filter_angle, filter_height, filter_width, filter_x, filter_y;
	int *pix = malloc(8*sizeof(int));
	int *anglebias = malloc(4*sizeof(int));
	//double *H_wp=malloc(16*sizeof(double));
	//double *H_pb=malloc(16*sizeof(double));
	double *pinpon=malloc(3*sizeof(double));
	int yaw;
    for (int i = 0; i < msg->nobjects; i += 1) {
        pixy_t *obj = &msg->objects[i];
		if(obj->signature == 012){
			filter_angle = mixedfilter(state->qraw_angle1, state->qmedian_angle1, obj->angle);
			filter_height = mixedfilter(state->qraw_height1, state->qmedian_height1, obj->height);
			filter_width = mixedfilter(state->qraw_width1, state->qmedian_width1, obj->width);
			filter_x = mixedfilter(state->qraw_x1, state->qmedian_x1, obj->x);
			filter_y = mixedfilter(state->qraw_y1, state->qmedian_y1, obj->y);
			if (msg->nobjects == 4){
				pix[0] = filter_x;
				pix[1] = filter_y;
				anglebias[0] = 90 - filter_angle;
			}
		}
		else if(obj->signature == 023){
			filter_angle = mixedfilter(state->qraw_angle2, state->qmedian_angle2, obj->angle);
			filter_height = mixedfilter(state->qraw_height2, state->qmedian_height2, obj->height);
			filter_width = mixedfilter(state->qraw_width2, state->qmedian_width2, obj->width);
			filter_x = mixedfilter(state->qraw_x2, state->qmedian_x2, obj->x);
			filter_y = mixedfilter(state->qraw_y2, state->qmedian_y2, obj->y);
			if (msg->nobjects == 4){
				pix[4] = filter_x;
				pix[5] = filter_y;
				anglebias[1] = -90 - filter_angle;
			}
			/*printf("Color code %d (octal %o) at (%d, %d) size (%d, %d)"
                   " angle %d\n", obj->signature, obj->signature,
                   filter_x, filter_y, filter_width, filter_height, filter_angle);*/
		}
		else if(obj->signature == 013){
			filter_angle = mixedfilter(state->qraw_angle3, state->qmedian_angle3, obj->angle);
			filter_height = mixedfilter(state->qraw_height3, state->qmedian_height3, obj->height);
			filter_width = mixedfilter(state->qraw_width3, state->qmedian_width3, obj->width);
			filter_x = mixedfilter(state->qraw_x3, state->qmedian_x3, obj->x);
			filter_y = mixedfilter(state->qraw_y3, state->qmedian_y3, obj->y);
			if (msg->nobjects == 4){
				pix[2] = filter_x;
				pix[3] = filter_y;
				anglebias[2] = 0 - filter_angle;
			}
		}
		else if(obj->signature == 024){
			filter_angle = mixedfilter(state->qraw_angle4, state->qmedian_angle4, obj->angle);
			filter_height = mixedfilter(state->qraw_height4, state->qmedian_height4, obj->height);
			filter_width = mixedfilter(state->qraw_width4, state->qmedian_width4, obj->width);
			filter_x = mixedfilter(state->qraw_x4, state->qmedian_x4, obj->x);
			filter_y = mixedfilter(state->qraw_y4, state->qmedian_y4, obj->y);
			if (msg->nobjects == 4){
				pix[6] = filter_x;
				pix[7] = filter_y;
				anglebias[3] = 2 - filter_angle;
			}
		}

		//printf("%d: ", i);
		//printf("Color code %d (octal %o) at (%d, %d) size (%d, %d)"
                  // " angle %d\n", obj->signature, obj->signature,
                   //filter_x, filter_y, filter_width, filter_height, filter_angle);
		
        
        /*if (obj->type == PIXY_T_TYPE_COLOR_CODE) {
            printf("Color code %d (octal %o) at (%d, %d) size (%d, %d)"
                   " angle %d\n", obj->signature, obj->signature,
                   filter_x, filter_y, filter_width, filter_height, filter_angle);
        } else {
            printf("Signature %d at (%d, %d) size (%d, %d)\n",
                   filter_x, filter_y, filter_width, filter_height);
        }*/
    }
if (msg->nobjects == 4){
			yaw = (anglebias[0] + anglebias[1] + anglebias[2] + anglebias[3]) / 4;
			//H_wp = trans_wp(pix, yaw);
			//H_pb = trans_pb();
			pinpon = trans_all(pix, yaw);

			printf("The manipulator should go to (%lf, %lf, %lf)\n", pinpon[0], pinpon[1], 
					pinpon[2]);
			//printf("Center[x] = %lf,\tCenter[y] = %lf\n", H_wp[3], H_wp[7]);
           // printf("H_pb[%lf,\t%lf\t,%lf]\n",H_pb[3], H_pb[7], H_pb[11]);
			printf("Yaw angle is %lf\n", yaw);
		
			state->msg_out.utime = utime_now();
			state->msg_out.goal_x = pinpon[0];
			state->msg_out.goal_y = pinpon[1];
			state->msg_out.goal_z = pinpon[2];

			goal_t_publish(state->lcm_out, state->channel, &(state->msg_out));
		}
}

void imu_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                   const imu_t *msg, void *userdata){
	state_t *state = userdata;
	state->imu = msg;
	double filtered_accel1, filtered_accel2, filtered_accel3, filtered_gyro1, filtered_gyro2, filtered_gyro3;
	filtered_accel1 = mixedfilter(state->qraw_accel1, state->qmedian_accel1, state->imu->accel[0]);
	filtered_accel2 = mixedfilter(state->qraw_accel2, state->qmedian_accel2, state->imu->accel[1]);
	filtered_accel3 = mixedfilter(state->qraw_accel3, state->qmedian_accel3, state->imu->accel[2]);
	filtered_gyro1 = mixedfilter(state->qraw_gyro1, state->qmedian_gyro1, state->imu->gyro[0]);
	filtered_gyro2 = mixedfilter(state->qraw_gyro2, state->qmedian_gyro2, state->imu->gyro[1]);
	filtered_gyro3 = mixedfilter(state->qraw_gyro3, state->qmedian_gyro3, state->imu->gyro[2]);
	
	//printf("Filtered accel(%lf, %lf, %lf), and filtered gyro(%lf, %lf, %lf)\n", filtered_accel1, filtered_accel2, filtered_accel3, filtered_gyro1, filtered_gyro2, filtered_gyro3);
	state->msg_out2.accel[0] = filtered_accel1;
	state->msg_out2.accel[1] = filtered_accel2;
	state->msg_out2.accel[2] = filtered_accel3;
	state->msg_out2.gyro[0] = filtered_gyro1;
	state->msg_out2.gyro[1] = filtered_gyro2;
	state->msg_out2.gyro[2] = filtered_gyro3;
	

	imu_t_publish(state->lcm_out2, state->channel2, &(state->msg_out2));
}

state_t *state_create (void){
	state_t *state = calloc (1, sizeof(*state));
	state->pixy = calloc(1,sizeof(pixy_t));
	state->lcm = lcm_create(NULL);
	state->pixy_frame = calloc(1,sizeof(pixy_frame_t));
	
	state->channel = "GOAL";
  state->lcm_out = lcm_create(NULL);
  state->msg_out.goal_x = 0;
  state->msg_out.goal_y = 0;
  state->msg_out.goal_z = 0;
  state->msg_out.utime = 0;
  
  state->channel2 = "IMUFILTERED";
  state->lcm_out2 = lcm_create(NULL);
  state->msg_out2.accel[0] = 0;
  state->msg_out2.accel[1] = 0;
  state->msg_out2.accel[2] = 0;
  state->msg_out2.gyro[0] = 0;
  state->msg_out2.gyro[1] = 0;
  state->msg_out2.gyro[2] = 0;
	
	state->qraw_angle1 = QueueCreate();
	state->qmedian_angle1 = QueueCreate();
	state->qraw_height1 = QueueCreate();
	state->qmedian_height1 = QueueCreate();
	state->qraw_x1 = QueueCreate();
	state->qmedian_x1 = QueueCreate();
	state->qraw_y1 = QueueCreate();
	state->qmedian_y1 = QueueCreate();
	state->qraw_width1 = QueueCreate();
	state->qmedian_width1 = QueueCreate();

	state->qraw_angle2 = QueueCreate();
	state->qmedian_angle2 = QueueCreate();
	state->qraw_height2 = QueueCreate();
	state->qmedian_height2 = QueueCreate();
	state->qraw_x2 = QueueCreate();
	state->qmedian_x2 = QueueCreate();
	state->qraw_y2 = QueueCreate();
	state->qmedian_y2 = QueueCreate();
	state->qraw_width2 = QueueCreate();
	state->qmedian_width2 = QueueCreate();

	state->qraw_angle4 = QueueCreate();
	state->qmedian_angle4 = QueueCreate();
	state->qraw_height4 = QueueCreate();
	state->qmedian_height4 = QueueCreate();
	state->qraw_x4 = QueueCreate();
	state->qmedian_x4 = QueueCreate();
	state->qraw_y4 = QueueCreate();
	state->qmedian_y4 = QueueCreate();
	state->qraw_width4 = QueueCreate();
	state->qmedian_width4 = QueueCreate();

	state->qraw_angle3 = QueueCreate();
	state->qmedian_angle3 = QueueCreate();
	state->qraw_height3 = QueueCreate();
	state->qmedian_height3 = QueueCreate();
	state->qraw_x3 = QueueCreate();
	state->qmedian_x3 = QueueCreate();
	state->qraw_y3 = QueueCreate();
	state->qmedian_y3 = QueueCreate();
	state->qraw_width3 = QueueCreate();
	state->qmedian_width3 = QueueCreate();

	state->qraw_accel1 = QueueCreate();
	state->qraw_accel2 = QueueCreate();
	state->qraw_accel3 = QueueCreate();
	state->qraw_gyro1 = QueueCreate();
	state->qraw_gyro2 = QueueCreate();
	state->qraw_gyro3 = QueueCreate();

	state->qmedian_accel1 = QueueCreate();
	state->qmedian_accel2 = QueueCreate();
	state->qmedian_accel3 = QueueCreate();
	state->qmedian_gyro1 = QueueCreate();
	state->qmedian_gyro2 = QueueCreate();
	state->qmedian_gyro3 = QueueCreate();
	return state;
}
void * imu_loop(void *user){
	lcm_t * lcm1 = lcm_create(NULL);
	imu_t_subscribe(lcm1,"IMU",imu_handler, user);
	while(1){
		lcm_handle(lcm1);
	}
}

int main()
{
	state_t *state = state_create ();
	pixy_frame_t_subscribe(state->lcm, "PIXY", pixy_handler, state);
  pthread_create (&state->imu_thread, NULL, imu_loop, state);
	
    // Enter read loop
    while (1) {
        lcm_handle(state->lcm);
    }

    lcm_destroy(state->lcm);
}
